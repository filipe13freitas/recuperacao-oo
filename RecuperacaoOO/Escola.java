import java.util.ArrayList;
import java.util.TreeMap;

public class Escola {

    private ArrayList<Aluno> alunos;
    private ArrayList<Funcionario> funcionarios;
    private ArrayList<Turmas> turmas;

    public Escola(){
        alunos = new ArrayList<>();
        funcionarios = new ArrayList<>();
        turmas = new ArrayList<>();
    }

    public boolean cadastrarProfessor(String nome, String cpf){

        int codigo = funcionarios.size()+1;
        Professores  p = new Professores(codigo,nome,cpf);

        funcionarios.add(p);

        return true;
    }
    public boolean cadastrarAssistentes(String nome, String cpf){

        int codigo = funcionarios.size()+1;
        Assistente assist = new Assistente(codigo,nome,cpf);

        funcionarios.add(assist);

        return true;
    }

    public String listarProfessores(){
        String str="";

        for(Funcionario p:funcionarios){
            if (p instanceof Professores) {
                str += "\t" + p.toString() + "\n";
            }
        }

        return str;
    }
    public String listarAssistentes(){
        String str="";

        for(Funcionario assist:funcionarios){
            if (assist instanceof Assistente) {
                str += "\t" + assist.toString() + "\n";
            }
        }

        return str;
    }

    public boolean cadastrarAluno(String nome, String cpf, String telefone){

        int codigo = alunos.size()+1;
        Aluno a = new Aluno(codigo,nome,cpf);
        a.setTelefone(telefone);

        alunos.add(a);

        return true;
    }

    public String listarAlunos() {
        String str = "";

        for (Aluno a : alunos) {
            str += "\t" + a.toString() + "\n";
        }

        return str;
    }

    public boolean cadastrarTurma(int nivel, String lingua){
        int codigo = turmas.size()+1;
        Turmas a = new Turmas(codigo, nivel, lingua);

        turmas.add(a);

        return true;
    }

    public String listarTurma() {
        String str = "";

        for (Turmas j : turmas) {
            str += "\t" + j.toString() + "\n";
        }

        return str;
    }

    public Aluno buscarAluno(int codigo){
        for (int i=0; i<alunos.size(); i++){
            if(alunos.get(i).getCodigo() == codigo){
                return alunos.get(i);
            }
        }
        return null;
    }
    public Funcionario buscarProfessor(int codigo){

        for (Funcionario p: funcionarios){
            if(p instanceof Professores){
                if (p.getCodigo() == codigo){
                    return p;
                }
            }
        }
        return null;
    }

    public Funcionario buscarAssistente(int codigo){
        for (Funcionario p:funcionarios){
            if (p instanceof Assistente) {
                if (p.getCodigo() == codigo){
                    return p;
                }
            }
        }
        return null;
    }
}