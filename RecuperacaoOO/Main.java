import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int op;
        String nome,cpf,telefone;
        int nivel;
        String lingua;
        int codigo;

        Escola escola = new Escola();

        do{
            System.out.println("---Controle de Escola---");
            System.out.println("1 - Cadastrar Professores");
            System.out.println("2 - Listar Professores");
            System.out.println("3 - Cadastrar Assistentes");
            System.out.println("4 - Listar Assistentes");
            System.out.println("5 - Cadastrar Aluno");
            System.out.println("6 - Listar Alunos");
            System.out.println("7 - Cadastrar Turma");
            System.out.println("8 - Listar Turma");
            System.out.println("9 - Buscar aluno");
            System.out.println("10 - Buscar Professor");
            System.out.println("11 - Buscar Assistente");
            System.out.println("Digite:");
            op = scan.nextInt();
            scan.nextLine();

            switch (op){
                case 1:
                    System.out.println("");
                    System.out.println("Nome:");
                    nome = scan.nextLine();
                    System.out.println("Cpf:");
                    cpf = scan.nextLine();

                    escola.cadastrarProfessor(nome, cpf);
                    break;

                case 2:
                    System.out.println(escola.listarProfessores());
                    break;

                case 3:
                    System.out.println("");
                    System.out.println("Nome:");
                    nome = scan.nextLine();
                    System.out.println("Cpf:");
                    cpf = scan.nextLine();

                    escola.cadastrarAssistentes(nome, cpf);
                    break;

                case 4:
                    System.out.println("Listar assistentes: ");
                    System.out.println(escola.listarAssistentes());

                    break;

                case 5:
                    System.out.println("");
                    System.out.println("Nome:");
                    nome = scan.nextLine();
                    System.out.println("Cpf:");
                    cpf = scan.nextLine();
                    System.out.println("Telefone:");
                    telefone = scan.nextLine();

                    escola.cadastrarAluno(nome, cpf, telefone);
                    break;

                case 6:
                    System.out.println(escola.listarAlunos());
                    break;

                case 7:
                    System.out.println("Nível: ");
                    nivel = scan.nextInt();
                    scan.nextLine();
                    System.out.println("Lingua: ");
                    lingua = scan.nextLine();

                    escola.cadastrarTurma(nivel, lingua);
                    break;

                case 8:

                    System.out.println(escola.listarTurma());
                    break;
                case 9:
                    System.out.println("Codigo do Aluno: ");
                    codigo = scan.nextInt();
                    scan.nextLine();

                    escola.buscarAluno(codigo);
                    Aluno alunoBusca = escola.buscarAluno(codigo);

                    if(alunoBusca != null){
                        System.out.println(alunoBusca);
                    }
                    else{
                        System.out.println("Não existe esse aluno");
                    }
                    break;
                case 10:
                    System.out.println("Codigo do Professor: ");
                    codigo = scan.nextInt();
                    scan.nextLine();

                    escola.buscarAluno(codigo);
                    Funcionario professorBusca = escola.buscarProfessor(codigo);

                    if(professorBusca != null){
                        System.out.println(professorBusca);
                    }
                    else{
                        System.out.println("\nNão existe Professor com esse nome\n");
                    }
                break;

                case 11:
                    System.out.println("Codigo do Assistente: ");
                    codigo = scan.nextInt();
                    scan.nextLine();

                    escola.buscarAluno(codigo);
                    Funcionario assistenteBusca = escola.buscarAssistente(codigo);

                    if(assistenteBusca != null){
                        System.out.println(assistenteBusca);
                    }
                    else{
                        System.out.println("Não existe Assistente com este código");
                    }
            }

        }while(op != 0);

    }
}
