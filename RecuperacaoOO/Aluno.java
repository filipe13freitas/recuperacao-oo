public class Aluno {


    private int codigo;
    private String nome;
    private String cpf;
    private String telefone;
    public Aluno(int codigo, String nome,String cpf){
        this.codigo=codigo;
        this.nome =nome;
        this.cpf = cpf;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String toString(){
        String str="";

        str+="Nome="+nome+" Cpf="+cpf+" Telefone="+telefone;

        return str;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
