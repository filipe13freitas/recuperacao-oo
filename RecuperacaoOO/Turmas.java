import java.util.ArrayList;

public class Turmas {

    private int nivel;
    private String lingua;
    private int codigo;
    ArrayList<Aluno> alunos;

    public Turmas(int codigo,int nivel, String lingua) {
        this.nivel = nivel;
        this.lingua = lingua;
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Turmas{" +
                "nivel=" + nivel +
                ", lingua='" + lingua + '\'' +
                '}';
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getLingua() {
        return lingua;
    }

    public void setLingua(String lingua) {
        this.lingua = lingua;
    }
}